package jhyun.sih.tests.my_first_message_source_advice;

import jhyun.sih.concepts.MyFirstMessageSourceAdvice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Matchers.any;

/**
 * Created by JohnWindows on 2016-03-16.
 */
@ImportResource({"classpath:my-first-message-source-advice.xml"})
@SpringBootApplication
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MyFirstMessageSourceAdviceTests.class,})
public class MyFirstMessageSourceAdviceTests {

    private static final Logger LOG = LoggerFactory.getLogger(MyFirstMessageSourceAdviceTests.class);

    @Qualifier("advicedChannel")
    @Autowired
    private MessageChannel channel;

    @Autowired
    private MyFirstMessageSourceAdvice spiedMyFirstMessageSourceAdvice;

    @Test
    public void t() throws Exception {
        MessagingTemplate mt = new MessagingTemplate(channel);
        mt.send(new GenericMessage<String>("Hey"));
        Thread.sleep(1000L);
    }

}
