package jhyun.sih.tests;

import jhyun.sih.concepts.SimpleNetApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageDeliveryException;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by JohnWindows on 2016-03-16.
 */
@ImportResource({"classpath:direct-channel-vs-rendezvous-channel.xml"})
@SpringBootApplication
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DirectChannelVsReRendezvousChannelTests.class,})
public class DirectChannelVsReRendezvousChannelTests {

    private static final Logger LOG = LoggerFactory.getLogger(DirectChannelVsReRendezvousChannelTests.class);

    @Autowired
    private MessageChannel lonelyDirectChannel;

    @Test
    public void tDirect() {
        assertNotNull(lonelyDirectChannel);
        MessagingTemplate messagingTemplate = new MessagingTemplate(lonelyDirectChannel);
        messagingTemplate.send(new GenericMessage<String>("Hey"));
    }

    @Autowired
    private MessageChannel lonelyRendezvousChannel;

    @Test
    public void tRendezvous() {
        assertNotNull(lonelyRendezvousChannel);
        MessagingTemplate messagingTemplate = new MessagingTemplate(lonelyRendezvousChannel);
        messagingTemplate.send(new GenericMessage<String>("Hey"));
    }

}
