package jhyun.sih.tests;

import jhyun.sih.concepts.HelloService;
import jhyun.sih.concepts.INothingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

/**
 * Created by JohnWindows on 2016-03-16.
 */
@ImportResource({"classpath:wire-tap.xml"})
@SpringBootApplication
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WireTapTests.class,})
public class WireTapTests {

    private static final Logger LOG = LoggerFactory.getLogger(WireTapTests.class);

    @Qualifier("nullChannel")
    @Autowired
    private MessageChannel nullChannel;

    @Autowired
    private INothingService nothingServiceMock;

    @Autowired
    private MessageChannel helloChannel;

    @Test
    public void t() throws Exception {
        MessagingTemplate nullMesgTmpl = new MessagingTemplate(nullChannel);
        nullMesgTmpl.send(new GenericMessage<String>("Hey"));
        verify(nothingServiceMock, never()).nop(any());
        //
        MessagingTemplate helloTmp = new MessagingTemplate(helloChannel);
        helloTmp.send(new GenericMessage<String>("Hey"));
        verify(nothingServiceMock, times(1)).nop("Hey");
    }

}
