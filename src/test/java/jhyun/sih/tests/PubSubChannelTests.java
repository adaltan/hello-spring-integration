package jhyun.sih.tests;

import jhyun.sih.concepts.HelloService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by JohnWindows on 2016-03-16.
 */
@ImportResource({"classpath:pub-sub-channel.xml"})
@SpringBootApplication
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PubSubChannelTests.class,})
public class PubSubChannelTests {

    private static final Logger LOG = LoggerFactory.getLogger(PubSubChannelTests.class);

    @Autowired
    private MessageChannel pubSubChannel;

    @Autowired
    private HelloService helloMockOne;

    @Autowired
    private HelloService helloMockTwo;

    @Test
    public void tPubSubSendOnce() throws Exception {
        assertNotNull(pubSubChannel);
        MessagingTemplate messagingTemplate = new MessagingTemplate(pubSubChannel);
        messagingTemplate.send(new GenericMessage<String>("Hey"));
        Thread.sleep(100);
        verify(helloMockOne, times(1)).sayHello(anyObject());
        verify(helloMockTwo, times(1)).sayHello("Hey");
    }

}
