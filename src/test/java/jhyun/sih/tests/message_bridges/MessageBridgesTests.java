package jhyun.sih.tests.message_bridges;

import jhyun.sih.concepts.HelloService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by JohnWindows on 2016-03-18.
 */
@ImportResource({"classpath:message-bridges.xml"})
@SpringBootApplication
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MessageBridgesTests.class,})
public class MessageBridgesTests {

    @Autowired
    private MessageChannel channel1;

    @Autowired
    private HelloService helloServiceMock;

    @Test
    public void t() throws Exception {
        MessagingTemplate mt = new MessagingTemplate(channel1);
        mt.send(new GenericMessage<Object>("Hey"));
        //
        verify(helloServiceMock, times(1)).sayHello("Hey");
    }

    public static void main(String... args) {
        SpringApplication.run(MessageBridgesTests.class, args);
    }

    @Component
    public static class MyCommandLineRunner implements CommandLineRunner {

        @Override
        public void run(String... args) throws Exception {
            while (true) {
                Thread.sleep(50);
            }
        }
    }
}
