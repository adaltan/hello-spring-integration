package jhyun.sih.tests.channel_adapters;

import jhyun.sih.concepts.FairytaleGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


/**
 * Created by JohnWindows on 2016-03-16.
 */
@ImportResource({"classpath:channel-adapters.xml"})
@SpringBootApplication
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ChannelAdaptersTests.class,})
public class ChannelAdaptersTests {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelAdaptersTests.class);

    @Autowired
    private FairytaleGenerator spiedFairytaleGenerator;

    @Test
    public void t() throws Exception {
        for (int n = 0; n < 10; n++) {
            Thread.sleep(50);
        }
        //
        verify(spiedFairytaleGenerator, atLeast(6)).sentence();
    }

}
