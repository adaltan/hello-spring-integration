package jhyun.sih.degrees;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by JohnWindows on 2016-03-13.
 */
public class DegreesXmlMain {
    public static void main(String... args) throws Exception {
        // NOTE: http://projects.spring.io/spring-integration/#qsdsl
        ApplicationContext ctx =
                new ClassPathXmlApplicationContext("sihSimpleDegress.xml");
        // Simple Service
        TempConverter converter =
                ctx.getBean("simpleGateway", TempConverter.class);
        System.out.println(converter.fahrenheitToCelcius(68.0f));
        // Web Service
//        converter = ctx.getBean("wsGateway", TempConverter.class);
//        System.out.println(converter.fahrenheitToCelcius(68.0f));
    }
}
