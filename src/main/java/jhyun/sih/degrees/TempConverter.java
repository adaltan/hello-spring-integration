package jhyun.sih.degrees;

/**
 * Created by JohnWindows on 2016-03-13.
 */
public interface TempConverter {
    float fahrenheitToCelcius(float fahren);
}
