package jhyun.sih.concepts;

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by JohnWindows on 2016-03-13.
 */
public class HelloService {

    private static final Logger LOG = LoggerFactory.getLogger(HelloService.class);

    private boolean failThis;

    private String prefix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public boolean isFailThis() {
        return failThis;
    }

    public void setFailThis(boolean failThis) {
        this.failThis = failThis;
    }

    public void sayHello(final String message) throws Exception {
        if (isFailThis()) {
            LOG.error("REACHED -- {}", getPrefix());
            throw new Exception("NO REASON.");
        } else {
            //LOG.warn("{} -- Hello, {}!", StringUtils.upperCase(getPrefix()), Strings.nullToEmpty(message));
            LOG.debug("{} -- Hello, {}!", StringUtils.upperCase(getPrefix()), Strings.nullToEmpty(message));
        }
    }

}
