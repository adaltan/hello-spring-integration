package jhyun.sih.concepts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.aop.AbstractMessageSourceAdvice;
import org.springframework.integration.core.MessageSource;
import org.springframework.messaging.Message;

/**
 * Created by JohnWindows on 2016-03-16.
 */
public class MyFirstMessageSourceAdvice extends AbstractMessageSourceAdvice {
    private static final Logger LOG = LoggerFactory.getLogger(MyFirstMessageSourceAdvice.class);

    @Override
    public boolean beforeReceive(MessageSource<?> source) {
        LOG.debug("beforeReceive / MessageSource = {}", source);
        return true;
    }

    @Override
    public Message<?> afterReceive(Message<?> result, MessageSource<?> source) {
        LOG.debug("afterReceive / Message = {}, MessageSource = {}", result, source);
        //
        return result;
    }
}
