package jhyun.sih.concepts;

import io.codearte.jfairy.Fairy;

/**
 * Created by JohnWindows on 2016-03-18.
 */
public class FairytaleGenerator {

    private static final Fairy fairy = Fairy.create();

    public String sentence() {
        return fairy.textProducer().sentence();
    }
}
