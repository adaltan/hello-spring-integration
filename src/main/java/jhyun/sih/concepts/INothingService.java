package jhyun.sih.concepts;

/**
 * Created by JohnWindows on 2016-03-18.
 */
public interface INothingService {
    void nop(Object obj);
}
