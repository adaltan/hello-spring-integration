package jhyun.sih.concepts;

import jhyun.sih.degrees.TempConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

/**
 * Created by JohnWindows on 2016-03-13.
 */
@ImportResource({"classpath:sihSimpleNet.xml"})
@SpringBootApplication
public class SimpleNetApp {

    public static void main(String... args) throws Exception {
        SpringApplication.run(SimpleNetApp.class, args);
    }

    @Component
    public static class MyRunner implements CommandLineRunner {

        @Autowired
        private ApplicationContext appContext;

        @Autowired
        private MessageChannel inputChannel;

        @Autowired
        private MessageChannel queuedChannel;

        @Autowired
        private MessageChannel pubSubChannel;

        public void run(String... args) throws InterruptedException {
            MessagingTemplate inputChannelMessagingTemplate = new MessagingTemplate(inputChannel);
            for (int n = 0; n < 5; n++) {
                inputChannelMessagingTemplate.send(new GenericMessage<String>(String.format("foobar!(%d)", n)));
            }
            //
            /*
            MessagingTemplate queuedChannelMessagingTemplate = new MessagingTemplate(queuedChannel);
            for (int n = 0; n < 5; n++) {
                queuedChannelMessagingTemplate.send(new GenericMessage<String>(String.format("foobar!(%d)", n)));
            }
            */
            //
            MessagingTemplate pubSubChannelMessagingTemplate = new MessagingTemplate(pubSubChannel);
            for (int n = 0; n < 5; n++) {
                pubSubChannelMessagingTemplate.send(new GenericMessage<String>(String.format("pub-sub foobar!(%d)", n)));
            }
            //
            Thread.sleep(10 * 1_000);
            SpringApplication.exit(appContext, () -> 0);
        }
    }
}
